Feature: Patch Data Driven API 

Scenario Outline: Trigger Data Driven Patch APi request with valid request paramaters
		Given Enter <"Name"> and <"Job"> in Data Driven Patch requestbody
		When Send the Data Driven Patch request with payload
		Then Validate Data Driven Patch statuscode
		And Validate Data Driven Patch responsebody paramaters
		
Examples:
		|Name |job |
		|Anjali |Hr |
		|Ankita |AVP |
		|Arpita |Associate |	