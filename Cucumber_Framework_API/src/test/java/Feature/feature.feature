Feature: Trigger the API
@Post
Scenario: Trigger POST API request with valid request parameters
       Given Enter NAME and JOB in Post requestbody
       When Send the Post request with payload
       Then Validate Post statuscode 
       And Validate Post responsebody parameters
       
 @Put      
Scenario: Trigger PUT API request with valid request parameters
		Given Enter NAME and JOB in Put requestbody
		When Send the Put request with payload
		Then Validate Put statuscode
		And Validate Put responsebody paramaters

@Patch		
Scenario: Trigger Patch APi request with valid request paramaters
		Given Enter Name and Job in Patch requestbody
		When Send the Patch request with payload
		Then Validate Patch statuscode
		And Validate Patch responsebody paramaters

@Get
Scenario: Trigger Get Api request with valid request paramaters
		Given Validate the Get request endpoint
		When send the Get request
		Then Validate Get statuscode