Feature: Put Data Driven APi Testing

Scenario Outline: Trigger Data Driven PUT API request with valid request parameters
		Given Enter <"Name"> and <"Job"> in Data Driven Put requestbody
		When Send the Data Driven Put request with payload
		Then Validate Data Driven Put statuscode
		And Validate Data Driven Put responsebody paramaters
Examples:
		|Name |Job |
		|Mahesh |Manager |
		|Suresh |SrManager |
		|Naresh |TeamLead |