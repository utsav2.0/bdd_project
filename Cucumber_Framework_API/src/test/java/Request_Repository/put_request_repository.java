package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_Methods.Excel_Data_Extractor;

public class put_request_repository {
	public static String put_repository_tc1() throws IOException {
		ArrayList<String> Data =Excel_Data_Extractor.Excel_data_reader("Test_data", "Put_API", "put_tc3");
		//System.out.println(Data);
		String name=Data.get(1);
		String job=Data.get(2);
		
		String requestobody="{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestobody;
	}

}
