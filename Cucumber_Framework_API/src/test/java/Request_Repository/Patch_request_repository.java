package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_Methods.Excel_Data_Extractor;

public class Patch_request_repository {
	public static String patch_request_tc1() throws IOException {
		ArrayList<String> Data =Excel_Data_Extractor.Excel_data_reader("Test_data", "Patch_API", "patch_tc2");
	//	System.out.println(Data);
		String name=Data.get(1);
		String job=Data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	}
}
