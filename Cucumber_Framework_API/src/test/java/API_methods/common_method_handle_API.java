package API_methods;

import static io.restassured.RestAssured.given;

public class common_method_handle_API {

	// Post

	public static int post_statuscode(String requestbody, String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when()
				.post(endpoint).then().extract().statusCode();
		return statuscode;
	}

	public static String post_responseBody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;

	}

	// Put
	public static int put_statuscode(String requestbody, String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().put(endpoint)
				.then().extract().statusCode();
		return statuscode;
	}

	public static String put_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().put(endpoint)
				.then().extract().response().asString();
		return responsebody;
	}

	// Patch
	public static int patch_statuscode(String requestbody, String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().patch(endpoint)
				.then().extract().statusCode();
		return statuscode;
	}

	public static String patch_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.patch(endpoint).then().extract().asString();
		return responsebody;
	}

	// Get
	public static int get_statuscode(String endpoint) {
		int statuscode = given().when().get(endpoint).then().extract().statusCode();
		return statuscode;
	}

	public static String get_responsebody(String endpoint) {
		String responsebody = given().when().get(endpoint).then().extract().response().asString();
		return responsebody;
	}
	//Delete
	
	public static int delete_statuscode(String endpoint) {
		int statuscode=given().when().delete(endpoint).then().extract().statusCode();
				return statuscode;
		}
	}
	

