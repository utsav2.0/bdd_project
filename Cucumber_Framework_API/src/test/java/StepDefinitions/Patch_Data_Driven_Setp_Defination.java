package StepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_methods.common_method_handle_API;
import Endpoint.patch_endpoint;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Patch_Data_Driven_Setp_Defination {
	static String requestbody;
	static String endpoint;
	static int statuscode;
	static String responsebody;
	static File log_dir;

	@Given("Enter <{string}> and <{string}> in Data Driven Patch requestbody")
	public void enter_and_in_data_driven_patch_requestbody(String reqname, String reqjob) {
		log_dir = Handle_Directory.create_log_directory("Patch_DD_Log_Directory");
		endpoint = patch_endpoint.patch_endpont_TC1();
		requestbody = "{\r\n" + "    \"name\": \"" + reqname + "\",\r\n" + "    \"job\": \"" + reqjob + "\"\r\n" + "}";
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Data Driven Patch request with payload")
	public void send_the_data_driven_patch_request_with_payload() throws IOException {
		statuscode = common_method_handle_API.patch_statuscode(requestbody, endpoint);
		responsebody = common_method_handle_API.patch_responsebody(requestbody, endpoint);
		Handle_Api_logs.evidence_creator(log_dir, "Patch_DDTestCase_Logs", endpoint, requestbody, responsebody);
		System.out.println(responsebody);
	//	throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Data Driven Patch statuscode")
	public void validate_data_driven_patch_statuscode() {
		Assert.assertEquals(statuscode, 200);
	//	throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Data Driven Patch responsebody paramaters")
	public void validate_data_driven_patch_responsebody_paramaters() {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime date = LocalDateTime.now();
		String curr_date = date.toString().substring(0, 10);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_uptat = jsp_res.getString("updatedAt");
		res_uptat.substring(0, 10);

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(res_job, req_job);
//		Assert.assertEquals(curr_date, res_uptat);

		System.out.println(" ...... Patch Data Driven Validation Sucessfull ...");
	//	throw new io.cucumber.java.PendingException();
		// throw new io.cucumber.java.PendingException();
	}

}
