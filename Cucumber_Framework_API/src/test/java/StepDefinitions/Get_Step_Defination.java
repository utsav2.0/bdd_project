package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import API_methods.common_method_handle_API;
import Endpoint.get_endpoint;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get_Step_Defination {

	static String endpoint;
	static int statuscode;
	static String responsebody;
	static File log_dir;

	@Given("Validate the Get request endpoint")
	public void validate_the_get_request_endpoint() {
		log_dir = Handle_Directory.create_log_directory("Get_Log_Directory");
		endpoint = get_endpoint.get_endpoint_tc1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("send the Get request")
	public void send_the_get_request() throws IOException {

		statuscode = common_method_handle_API.get_statuscode(endpoint);
		responsebody = common_method_handle_API.get_responsebody(endpoint);
		Handle_Api_logs.evidence_creator(log_dir, "Get_TestCase_Logs", endpoint, null, responsebody);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get statuscode")
	public void validate_get_statuscode() {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(statuscode, 200);
		int excp_id[] = { 1, 2, 3, 4, 5, 6 };
		String excp_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String excp_fname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String excp_lname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		JSONObject res_array = new JSONObject(responsebody);
		JSONArray data_array = res_array.getJSONArray("data");
		int count = data_array.length();

		for (int i = 0; i < count; i++) {
			int id = excp_id[i];
			String email = excp_email[i];
			String fname = excp_fname[i];
			String lname = excp_lname[i];

			int res_id = data_array.getJSONObject(i).getInt("id");
			String res_email = data_array.getJSONObject(i).getString("email");
			String res_fname = data_array.getJSONObject(i).getString("first_name");
			String res_lname = data_array.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, id);
			Assert.assertEquals(res_email, email);
			Assert.assertEquals(res_fname, fname);
			Assert.assertEquals(res_lname, lname);

			// throw new io.cucumber.java.PendingException();
		}
		System.out.println("Get Validation sucessfull");

	}
}
