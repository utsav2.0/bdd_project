package StepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_methods.common_method_handle_API;
import Endpoint.put_endpoint;
import Request_Repository.put_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_Step_Defination {
	static String requestbody;
	static String endpoint;
	static int statuscode;
	static String responsebody;
	static File log_dir;
	
	@Given("Enter NAME and JOB in Put requestbody")
	public void enter_name_and_job_in_put_requestbody() throws IOException {
		log_dir=Handle_Directory.create_log_directory("Put_Log_Directory");
		endpoint=put_endpoint.put_endpoint_tc1();
		requestbody=put_request_repository.put_repository_tc1();
		

	  //  throw new io.cucumber.java.PendingException();
	}
	@When("Send the Put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		statuscode=common_method_handle_API.put_statuscode(requestbody, endpoint);
		responsebody=common_method_handle_API.put_responsebody(requestbody, endpoint);
		 Handle_Api_logs.evidence_creator(log_dir, "Put_TestCase_Logs", endpoint, requestbody, responsebody);
		
	  //  throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Put statuscode")
	public void validate_put_statuscode()  {
	    Assert.assertEquals(statuscode, 200);
	  //  throw new io.cucumber.java.PendingException();
	   
	}
	@Then("Validate Put responsebody paramaters")
	public void validate_put_responsebody_paramaters() {
	  JsonPath jsp_req=new JsonPath(requestbody);
	  String req_name=jsp_req.getString("name");
	  String req_job=jsp_req.getString("job");
	 LocalDateTime date=LocalDateTime.now();
	 String curr_date=date.toString().substring(0, 10);
	 
	 JsonPath jsp_res=new JsonPath(responsebody);
	 String res_name=jsp_res.getString("name");
	 String res_job=jsp_res.getString("job");
	
	 String upd_At=jsp_res.getString("updatedAt");
	 upd_At.substring(0, 10);
	
	Assert.assertEquals(req_name, res_name);
	Assert.assertEquals(res_job, req_job);
//Assert.assertEquals(curr_date, upd_At);
	
	
	System.out.println(" ...... Put Validation Sucessfull ...");
	}


}
