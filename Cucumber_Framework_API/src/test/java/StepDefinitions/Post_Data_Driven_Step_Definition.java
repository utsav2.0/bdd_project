package StepDefinitions;

import org.testng.Assert;

import API_methods.common_method_handle_API;
import Endpoint.post_endpoint;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;

import java.io.File;
import java.io.IOException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Post_Data_Driven_Step_Definition {

	String requestbody;
	int statuscode;
	String responsebody;
	String endpoint;
	File log_dir;

	@Given("Enter {string} and {string} in Post requestbody")
	public void enter_and_in_post_requestbody(String reqname, String reqjob) {
		log_dir = Handle_Directory.create_log_directory("POST_DD_Log_Directory");
		endpoint = post_endpoint.post_endpoint_TC1();
		requestbody = "{\r\n" + "    \"name\": \"" + reqname + "\",\r\n" + "    \"job\": \"" + reqjob + "\"\r\n" + "}";
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Data_Driven_Post request with Data_Driven_Payload")
	public void send_the_data_driven_post_request_with_data_driven_payload() throws IOException {
		statuscode = common_method_handle_API.post_statuscode(requestbody, endpoint);
		responsebody = common_method_handle_API.post_responseBody(requestbody, endpoint);
		Handle_Api_logs.evidence_creator(log_dir, "Post_DDTestCase_Logs", endpoint, requestbody, responsebody);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Data_Driven_Post statuscode")
	public void validate_data_driven_post_statuscode() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Data_Driven_Post responsebody parameters")
	public void validate_data_driven_post_responsebody_parameters() {
		JsonPath jsprequest = new JsonPath(requestbody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responsebody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Post Data Driven Response Body Validation Successfull");
		// throw new io.cucumber.java.PendingException();
	}

}
