package StepDefinitions;

import org.testng.Assert;

import API_methods.common_method_handle_API;
import Endpoint.post_endpoint;
import Request_Repository.Post_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;

import java.io.File;
import java.io.IOException;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Post_Step_Definition {
	
		String requestbody;
		int statuscode;
		String responsebody;
		String endpoint;
		File log_dir;
		
		@Before("@Post")
		public void On_start() {
			System.out.println("--- Post Step Defination Execution Started ----");
			log_dir=Handle_Directory.create_log_directory("Post_Log_Directory");
		}

		@Given("Enter NAME and JOB in Post requestbody")
		public void enter_name_and_job_in_request_body() throws IOException {
		//	log_dir=Handle_Directory.create_log_directory("Post_Log_Directory");
			endpoint=post_endpoint.post_endpoint_TC1();
			requestbody=Post_request_repository.post_request_tc1();
			//throw new io.cucumber.java.PendingException();
		}
		@Before("@Post")
		public void Trigger() {
			System.out.println("Post Api Triggered");
		}

		@When("Send the Post request with payload")
		public void send_the_request_with_payload() throws IOException {
			statuscode =common_method_handle_API.post_statuscode(requestbody, endpoint);
			responsebody =common_method_handle_API.post_responseBody(requestbody, endpoint);
			Handle_Api_logs.evidence_creator(log_dir, "Post_TestCase_Logs", endpoint, requestbody, responsebody);
			System.out.println(responsebody);
			
			//throw new io.cucumber.java.PendingException();
		}
		
		@Before(order=2)
		public void Response() {
			System.out.println("Post Api Response Received");
		}

		@Then("Validate Post statuscode")
		public void validate_status_code() {
			Assert.assertEquals(statuscode, 201);
					//throw new io.cucumber.java.PendingException();
		}
		
		@Before(order=3)
		public void statuscode() {
			System.out.println("Post Statuscode validation suscess");
		}
		@Then("Validate Post responsebody parameters")
		public void validate_response_body_parameters() {
			JsonPath jsprequest = new JsonPath(requestbody);
			String req_name = jsprequest.getString("name");
			String req_job = jsprequest.getString("job");
			JsonPath jspresponse = new JsonPath(responsebody);
			String res_name=jspresponse.getString("name");
			String res_job=jspresponse.getString("job");
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
		//	System.out.println("Post Responsebody Validation Successfull");
			//throw new io.cucumber.java.PendingException();
		}
	
		@After("@Post")
		public void tearDown() {
			System.out.println("--- Post Step Defination Execution Sucessfull ----");
		}
		

	}


