package cucumber.Options;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features={"src/test/java/Feature"}, glue= "StepDefinitions", tags="@Post or @Patch",plugin= {"pretty","html:target/Cucumber-Report1.html"})



public class Test_Runner {

}
