Feature: Post_Data_Driven

Scenario Outline: Trigger POST API request with valid request parameters
       Given Enter "<Name>" and "<Job>" in Post requestbody
       When Send the Data_Driven_Post request with Data_Driven_Payload
       Then Validate Data_Driven_Post statuscode 
       And Validate Data_Driven_Post responsebody parameters

Examples:
		|Name |Job |
		|Utsav |QA |
		|Rajesh |Java |
		|Mohan |BA |