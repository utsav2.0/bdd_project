# BDD_Project

BDD Cucumber Framework Overview:

The BDD Cucumber Framework that I have developed comprises seven key components, each playing a vital role in ensuring a robust and organized test automation structure:

1.	Runner class 
2.	Feature file, 
3.	Step definitions, 
4.	Common function, 
5.	Data files
6.	Libraries,
7.  Reports

Runner Class:
The foundation of the framework is the Runner Class, responsible for executing the entire program. Annotations such as "@RunWith(Cucumber.class)" and "@CucumberOptions" are utilized to drive test execution. Configuration parameters like feature file locations, glue code for step definitions, tags, and plugins are specified in this class.

Feature File:
Feature files, identified by the ".feature" extension, play a pivotal role in scenario design. Using keywords like Given, When, Then, and But, high-level descriptions of test cases are written in plain text. These files serve as a communication bridge between non-technical stakeholders and the automated testing process.

Step Definitions:
Step Definitions translate the plain text in feature files into functional Java code. Each step defined in the feature file corresponds to a method in the Step Definitions file. This separation enables the implementation of test logic while maintaining readability and clarity.

Common Function:
The Common Function component is further divided into two categories. The first category comprises API-related common functions, handling tasks such as triggering API calls, fetching status codes, and retrieving response bodies. The second category encompasses utility-related common functions, including automatic directory creation for test execution, creation of API logs in text files, and reading data from Excel files using Apache-POI libraries.

Data Files:
Data Files consist of Excel files that store data for test scenarios. The framework utilizes Apache-POI libraries to read and write data, providing flexibility and ease of maintenance.

Libraries:
The framework leverages various libraries to enhance its capabilities. Rest Assured is employed for API interactions, while TestNG is utilized for assertion and validation of response parameters. Additionally, Apache-POI libraries support reading and writing from Excel files. Cucumber Libraries assist in creating feature files, step definitions, and driving test execution.

Reports:
BDD Cucumber generates reports automatically, saving them in the project folder. Plugins can be incorporated to generate diverse types of reports, enhancing visibility into test results and facilitating comprehensive analysis.

Conclusion:
In conclusion, the BDD Cucumber Framework's well-structured components collectively contribute to effective test automation, fostering collaboration between technical and non-technical stakeholders. The integration of libraries and the automated report generation process further solidify the framework's efficiency and reliability.